/**
 * This class ....
 * 
 * @author someone
 */

public class OutInstruction extends Instruction {
	private int register;
	private int value;

	public OutInstruction(String label, String opcode) {
		super(label, opcode);
	}

	public OutInstruction(String label, int register, int x, int y) {
		super(label, "out");
		this.register = register;

	}

	@Override
	public void execute(Machine m) {
		value = ( m.getRegisters()).getRegister(register);
		// output value
		System.out.println(" register " + register + " value is " + value);
		
	}

	@Override
	public String toString() {
		return super.toString() + " output register " + register + " value ";
	}


}


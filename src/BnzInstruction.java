

/**
 * This class ....
 * 
 * @author someone
 */

public class BnzInstruction extends Instruction {

	private int result;
	private int op1;
	private int op2;

	public BnzInstruction(String label, String op) {
		super(label, op);
	}

	public BnzInstruction(String label, int result, int op1, int op2) {
		this(label, "bnz");
		this.result = result;
		this.op1 = op1;
//		this.op2 = op2;
	}

	@Override
	public void execute(Machine m) {
	
		// SECOND PARAMETER NOW IS IDENTIFIER WITHOUT FIRST SYMBOL f
		// IF IDENTIFIERS ARE WRONG (example "fds3" INSTEAD OF "f3") PROGRAM IS NOT CRUSHING BECAUSE OF scanInt(), ONLY THIS CLASS IS NOT WORKING PROPERLY
		int value1 = m.getRegisters().getRegister(result);
		if (value1 > 0){
		   m.setPc(op1);
		}
		
		
	}

	@Override
	public String toString() {
		return super.toString() + " " + op1 + " + " + op2 + " to " + result;
	}
}



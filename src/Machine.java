
import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.JFileChooser;





//import lombok.Data;
import lombok.*;

/*
 * The machine language interpreter
 */
@Data
public class Machine {
	// The labels in the SML program, in the order in which
	// they appear (are defined) in the program

	private Labels labels;
	
	private Labels getLabels() {
		// Auto-generated method stub
		return labels;
	}

	// The SML program, consisting of prog.size() instructions, each
	// of class Instruction (or one of its subclasses)
	private ArrayList<Instruction> prog;
	
	public ArrayList<Instruction> getProg() {
		 // Auto-generated method stub
		return prog;
	}

	 //The registers of the SML machine
	private Registers registers;
	
    Registers getRegisters() {
		//  Auto-generated method stub
		return registers;
	}
	

	
	private void setRegisters(Registers registers2) {
		//  Auto-generated method stub
		this.registers = registers2;
	}
	
	
//	public int getRegisters(int op1) {
//		// TODO Auto-generated method stub
//		return registers;
//	}
//
//	public void setRegisters(int result, int i) {
//		// TODO Auto-generated method stub
//		this.registers = registers2;
//	}
   

	// The program counter; it contains the index (in prog) of
	// the next instruction to be executed.

	public int pc=0;
	
	public void setPc(int i) {
		//  Auto-generated method stub
		this.pc= i;
		
	}

	public int getPc() {
		// Auto-generated method stub
		return pc;
	}
	

	{
		labels = new Labels();
		prog = new ArrayList<>();
		pc = 0;
	}

	public static void main(String [] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException, IOException  {
		System.out.println("main filename is "+ args[0]);
		Machine m = new Machine();
		//Translator t = new Translator();
		Translator t = new Translator(args[0]);
		String message= "Program File Name ";
		t.readAndTranslate(m.getLabels(), m.getProg());

		System.out.println("Here is the program; it has " + m.getProg().size()
				+ " instructions.");
		System.out.println(m);

		System.out.println("Beginning program execution.");
		m.execute();
		System.out.println("Ending program execution.");

		System.out.println("Values of registers at program termination:");
		System.out.println(m.getRegisters() + ".");
	}

	// Print the program

	


	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		
			for (int i = 0; i != getProg().size(); i++)
				s.append(getProg().get(i) + "\n");
	
		
	
		return s.toString();
	}
	

	// Execute the program in prog, beginning at instruction 0.
	// Precondition: the program and its labels have been store properly.


		
		
		//return null;
	//}

	public void execute() throws IOException {
		setPc(0);
		setRegisters(new Registers());
		//System.out.println(getRegisters());
		while (getPc() < getProg().size()) {
			Instruction ins = getProg().get(getPc());
			setPc(getPc() + 1);
			ins.execute(this);
		}
	}








	
	
}


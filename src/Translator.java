

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Scanner;

/*
 * The translator of a <b>S</b><b>M</b>al<b>L</b> program.
 */
public class Translator {

	// word + line is the part of the current line that's not yet processed
	// word has no whitespace
	// If word and line are not empty, line begins with whitespace
	private String line = "";
	private Labels labels; // The labels of the program being translated
	private ArrayList<Instruction> program; // The program to be created
	private String fileName; // source file of SML code
	private static final String SRC = "src";
	
	public Translator(String args) {
		//this.fileName = "D:\\data.dat";
		this.fileName =args;
		System.out.print("this filename is "+fileName);
	}

	// translate the small program in the file into lab (the labels) and
	// prog (the program)
	// return "no errors were detected"
	public boolean readAndTranslate(Labels lab, ArrayList<Instruction> prog) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		//System.out.println("File: IO error to start "+ prog);
		System.out.println();
		
		Scanner sc; // Scanner attached to the file chosen by the user
		try {
			sc = new Scanner(new File(fileName));
		} catch (IOException ioE) {
			System.out.println("File: IO error to start " + ioE.getMessage());
			return false;
		}
		labels = lab;;
		labels.reset();
		program = prog;
		program.clear();

		try {
			line = sc.nextLine();
		} catch (NoSuchElementException ioE) {
			return false;
		}

		// Each iteration processes line and reads the next line into line
		while (line != null) {
			// Store the label in label
			String label = scan();

			if (label.length() > 0) {
				Instruction ins = getInstruction(label);
				if (ins != null) {
					labels.addLabel(label);
					program.add( (Instruction) ins);
				}
			}

			try {
				line = sc.nextLine();
			} catch (NoSuchElementException ioE) {
				return false;
			}
		}
		return true;
	}

	// line should consist of an MML instruction, with its label already
	// removed. Translate line into an instruction with label label
	// and return the instruction
	public Instruction getInstruction(String label) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		int s1; // Possible operands of the instruction
		int s2;
		int r;
		

		if (line.equals(""))
			return null;

		String ins = scan();
		
		r = scanInt();
		s1 = scanInt();
		s2 = scanInt();
		
	// REFLECTION. NOW TO EXTED FUNCTIONALITY IS ONLY NEEDED TO FOLLOW THE CLASS NAMING SETTINGS	
		String classname = ins.substring(0, 1).toUpperCase() + ins.substring(1) + "Instruction";		
		Constructor<?> c = Class.forName(classname).getConstructor(String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE);
		return (Instruction) c.newInstance(label, r, s1, s2);
		
	// PREVIOUS EXAMPLE WITH SWITCH SCENARIO	
//		try {
//			r = scanInt();
//			s1 = scanInt();
//			s2 = scanInt();
//			return Class.forName(classname + "Instruction");
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
		
		
//		switch (ins) {
//		case "add":
//			r = scanInt();
//			s1 = scanInt();
//			s2 = scanInt();
//			return new DivInstruction(label, r, s1, s2);
//		case "mul":
//			r = scanInt();
//			s1 = scanInt();
//			s2 = scanInt();
//			return new MulInstruction(label, r, s1, s2);
//		case "sub":
//			r = scanInt();
//			s1 = scanInt();
//			s2 = scanInt();
//			return new SubInstruction(label, r, s1, s2);
//		case "add":
//			r = scanInt();
//			s1 = scanInt();
//			s2 = scanInt();
//			return new AddInstruction(label, r, s1, s2);
//		case "out":
//			r = scanInt();
//			s1 = scanInt();
//			return new OutInstruction(label, r);
//		case "lin":
//			r = scanInt();
//			s1 = scanInt();
//			return new LinInstruction(label, r, s1);
//		}
        
		// You will have to write code here for the other instructions.

//		return null;
//		
	}
    
	/*
	 * Return the first word of line and remove it from line. If there is no
	 * word, return ""
	 */
	public String scan() {
		line = line.trim();
		if (line.length() == 0)
			return "";

		int i = 0;
		while (i < line.length() && line.charAt(i) != ' '
				&& line.charAt(i) != '\t') {
			i = i + 1;
		}
		String word = line.substring(0, i);
		line = line.substring(i);
		return word;
	}

	// Return the first word of line as an integer. If there is
	// any error, return the maximum int
	public int scanInt() {
		
		String word = scan();
		if (word.length() == 0) {
			return Integer.MAX_VALUE;
		}
		
		/// THIS IS HACK. CHECKING IF CONSTRUCTRUCTION ARGUMENT FOR BnzInstruction CLASS IS IDEENTIFIER
		if (word.startsWith("f")) {
			word = word.substring(1);
		}     
		///
		
		try {
			return Integer.parseInt(word);
		} catch (NumberFormatException e) {
			return Integer.MAX_VALUE;
		}

	}
}
